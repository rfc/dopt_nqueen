import ctypes
import numpy
from numpy.ctypeslib import ndpointer
import os

DEFAULT_LIBRARY_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'libjungle.so')

class Solver:
    def __init__(self, lib_path=None):
        self._lib_path = lib_path or DEFAULT_LIBRARY_PATH

        self._lib = ctypes.CDLL(self._lib_path)

        print self._lib

        self._solve = self._lib.solve_queens
        self._solve.argtypes = [
            ctypes.c_int64,
            ctypes.c_int64,
            ndpointer(dtype=numpy.int64),
        ]
        self._solve.restype = ctypes.c_int

    def solve(self, n, max_steps):
        out = numpy.zeros((n, ), dtype=numpy.int64)
        rv = self._solve(n, max_steps, out)
        if rv:
            raise RuntimeError('no solution')
        assert rv == 0
        return out

