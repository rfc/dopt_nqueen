#!/usr/bin/python
# -*- coding: utf-8 -*-

def solveIt(n):
    import cjungle
    solver = cjungle.Solver()
    sol = list(solver.solve(n, max_steps=1099511627776))

    assert len(sol) == len(set(sol))
    assert None not in sol
    
    # prepare the solution in the specified output format
    # if no solution is found, put 0s
    outputData = str(n) + '\n'
    if sol == None:
        print 'no solution found.'
        outputData += ' '.join(map(str, [0]*n))+'\n'
    else: 
        outputData += ' '.join(map(str, sol))+'\n'
        
    return outputData

import sys
import argparse

def parse_args(argv):
    p = argparse.ArgumentParser()
    p.add_argument('n', nargs=1, type=int)
    p.add_argument('--profile', action='store_const', const=True, default=False)
    return p.parse_args()

if __name__ == "__main__":
    args = parse_args(sys.argv[1:])

    def body():
        n = args.n[0]
        print 'Solving Size:', n
        print(solveIt(n))

    if args.profile:
        import cProfile
        import pstats

        p = cProfile.Profile()
        try:
            p.runcall(body)
        except KeyboardInterrupt:
            pass
        s = pstats.Stats(p)
        s.sort_stats('cumulative')
        s.print_stats(20)
    else:
        body()


