#include <vector>
#include <algorithm>
#include <stdio.h>
#include <assert.h>
#include <math.h>

typedef long int index_t;
typedef std::vector<index_t> v_t;

struct x_t {
    index_t n;
    index_t max_attempts;
    v_t queen_row;
    v_t z_1, z_2, z_3, buff_1, buff_2;
    index_t obj;

    x_t(index_t n_, index_t max_attempts_) : n(n_), max_attempts(max_attempts_),
        queen_row(n_, 0), z_1(n_, 0), z_2(2*n_, 0), z_3(2*n_, 0),
        buff_1(n_, 0), buff_2(n_, 0), obj(0) {}

    inline void add_queen(index_t i, index_t j) {
        queen_row[i] = j;
        z_1[j] += 1;
        z_2[n + i - j] += 1;
        z_3[i + j] += 1;
    }

    inline void move_queen(index_t i, index_t j) {
        index_t prev_j = queen_row[i];
        z_1[prev_j] -= 1;
        z_2[n + i - prev_j] -= 1;
        z_3[i + prev_j] -= 1;
        add_queen(i, j);
    }

    inline index_t conflicts(index_t i, index_t j) const {
        return 1 + z_1[j] + z_2[n + i - j] + z_3[i + j];
    }

    index_t recompute_objective() {
        index_t x = 0;
        for (index_t i = 0; i < n; ++i) {
            x += conflicts(i, queen_row[i]);
        }
        return x;
    }

    inline index_t delta(index_t i, index_t j_src, index_t j_dst) const {
        if (j_src == j_dst) {
            return (index_t)0;
        } else {
            return 2 * ((z_1[j_dst] - z_1[j_src] + 1) +
                (z_2[n + i - j_dst] - z_2[n + i - j_src] + 1) +
                (z_3[i + j_dst] - z_3[i + j_src] + 1));
        }
    }

    inline void conflicts(index_t i, v_t & out) {
        for (index_t j = 0; j < n; ++j) {
            out[j] = delta(i, queen_row[i], j);
        }
    }

    inline index_t min(v_t & a) {
        index_t x = a[0];
        for (index_t j = 1; j < n; ++j) {
            x = std::min(x, a[j]);
        }
        return x;
    }

    inline index_t choose(index_t x, v_t & a, v_t & temp) {
        index_t i = 0;
        for (index_t j = 0; j < n; ++j) {
            if (a[j] == x) {
                temp[i++] = j;
            }
        }
        assert(i > 0);
        assert(i <= n);
        return temp[rand() % i];
    }

    void construct() {
        for (index_t i = 0; i < n; ++i) {
            add_queen(i, 0);
            conflicts(i, buff_1);
            index_t j = choose(min(buff_1), buff_1, buff_2);
            move_queen(i, j);
            printf("placed queen %ld %ld\n", i, j);
        }
        obj = recompute_objective();
    }

    void randomly_construct() {
        for (index_t i = 0; i < n; ++i) {
            index_t j = (index_t)(rand() % n);
            add_queen(i, j);
        }
        obj = recompute_objective();
    }

    void dump() {
        printf("\n");
        for (index_t i = 0; i < n; ++i)
            printf(" Q %ld = %ld\n", i, queen_row[i]);

        printf("\n");
        for (index_t i = 0; i < n; ++i)
            printf("Z1 %ld = %ld\n", i, z_1[i]);

        printf("\n");
        for (index_t i = 0; i < 2*n; ++i)
            printf("Z2 %ld = %ld\n", i, z_2[i]);

        printf("\n");
        for (index_t i = 0; i < 2*n; ++i)
            printf("Z3 %ld = %ld\n", i, z_3[i]);

        for (index_t i = 0; i < n; ++i) {
            index_t j = queen_row[i];
            printf(" Q %ld | %ld %ld %ld %ld\n", i, (index_t)1, z_1[i], z_2[n + i - j], z_3[i + j]);
        }
    }

    void conflicting_queens(v_t & out) {
        for (index_t i = 0; i < n; ++i) {
            if (conflicts(i, queen_row[i]) > 4) {
                out[i] = 1;
            } else {
                out[i] = 0;
            }
        }
    }

    inline index_t random_conflicting_queen() {
        // attempt to stumble on a winner
        for (index_t attempts=0; attempts<max_attempts; ++attempts) {
            index_t i = (index_t)(rand() % n);
            if (conflicts(i, queen_row[i]) > 4)
                return i;
        }
        // directly enumerate all conflicting queens and sample
        conflicting_queens(buff_1);
        return choose(1, buff_1, buff_2);
    }

    inline index_t random_improving_row(index_t i) {
        // attempt to stumble on a winner
        for (index_t attempts=0; attempts<max_attempts; ++attempts) {
            index_t j = (index_t)(rand() % n);
            index_t d = delta(i, queen_row[i], j);
            if (d < 0) {
                return j;
            }
        }
        // directly enumerate all moves and sample
        conflicts(i, buff_1);
        return choose(min(buff_1), buff_1, buff_2);
    }

    void step() {
        // pick a random queen in conflict
        index_t i = random_conflicting_queen();
        // move to a random conflict-minimising row
        index_t j = random_improving_row(i);
        obj += delta(i, queen_row[i], j);
        move_queen(i, j);
    }
};

extern "C" {
int solve_queens(index_t n, index_t max_steps, index_t *result) {
    printf("solve_queens {n=%ld, max_steps=%ld, sizeof(index_t)=%ld}\n", n, max_steps, sizeof(index_t));
    index_t goal_obj = 4 * n;
    // this seems to work tolerably for n = 2 ** 20
    index_t max_attempts = std::max(n/11, (index_t)1);
    x_t x(n, max_attempts);
    printf("constructing...\n");
    x.randomly_construct();
    printf("intitial objective\t%8ld\n", x.obj);
    printf("    goal objective\t%8ld\n", goal_obj);
    if (x.obj < goal_obj) {
        printf("error during construction\n");
        return -1;
    }
    printf("searching...\n");
    for (index_t step = 0; step < max_steps; ++step) {
        if ((step % 1000) == 0) {
            printf("\tstep\t%8ld;\tenergy\t%8ld\n", step, (x.obj-goal_obj));
        }
        // x.dump();
        if (x.obj == goal_obj) {
            break;
        } else if (x.obj < goal_obj) {
            printf("error: objective is ludicrously low, something has broken\n");
            return -1;
        // } else if ((double)x.obj > ((double)goal_obj * 1.02)) {
        //    x.quickstep();
        } else {
            x.step();
        }
    }
    for (index_t i = 0; i < n; ++i) {
        result[i] = x.queen_row[i];
    }
    return x.obj != goal_obj;
}
}

