CC := g++

OPT_FLAGS := -O3 -march=native
WARN_FLAGS := -Wall -Wextra -Werror -pedantic -Wconversion -Wshadow

# ref : http://stackoverflow.com/questions/9577627/ubuntu-11-10-linking-perftools-library
WITH_PROFILING := -Wl,--no-as-needed -lprofiler -Wl,-as-needed

CPP_FLAGS := $(OPT_FLAGS) $(WARN_FLAGS) --std=c++0x -fPIC

LIBS := -lm -lstdc++

all: libjungle.so
.PHONY: all

libjungle.so:	jungle.o
	$(CC) $(CPP_FLAGS) $^ $(LIBS) -Wl,-soname,$@ $(WITH_PROFILING) -shared -o $@

%.o:	%.cpp
	$(CC) $(CPP_FLAGS) -fPIC -c $^

clean:
	rm -f libjungle.so
	rm -f jungle.o
	rm -f jungle.out
	rm -f main.out
	rm -f main.o
.PHONY: clean
