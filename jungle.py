"""
jungle - greedy local search solver for n queens problem
"""

from collections import namedtuple, defaultdict
from heapq import heappush, heappop
import random
from itertools import chain, izip



State = namedtuple('State', ['obj', 'n', 'queens', 'depth'])
Tracker = namedtuple('Tracker', ['row_qc', 'col_qc', 'diag1_qc', 'diag2_qc'])


def gen_neighbours(n, queen):
    j = queen
    for j_prime in xrange(n):
        if j_prime != j:
            yield j_prime


def measure_conflicts(tr, (a, b, c, d)):
    return ((tr.row_qc[a]) + (tr.col_qc[b]) + (tr.diag1_qc[c]) + (tr.diag2_qc[d]))

def add_queen(tr, (a, b, c, d), x):
    tr.row_qc[a] += x
    tr.col_qc[b] += x
    tr.diag1_qc[c] += x
    tr.diag2_qc[d] += x


def normalise_queens(queens):
    return tuple(queens)


def locate_queen(n, i, queen):
    j = queen
    return (i, j, j + i, j - i)


def update_tracker(tr, old_loc, new_loc):
    tr_prime = Tracker(dict0(tr.row_qc), dict0(tr.col_qc), dict0(tr.diag1_qc), dict0(tr.diag2_qc))
    add_queen(tr_prime, old_loc, -1)
    add_queen(tr_prime, new_loc, 1)
    return tr_prime


def dict0(stuff=None):
    d = defaultdict(lambda : 0)
    if stuff is not None:
        d.update(stuff)
    return d


def dump_tracker(tracker):
    print 'row %s' % str(tracker.row_qc)
    print 'col %s' % str(tracker.col_qc)
    print 'di1 %s' % str(tracker.diag1_qc)
    print 'di2 %s' % str(tracker.diag2_qc)


def make_tracker(n, queens):
    tracker = Tracker(dict0(), dict0(), dict0(), dict0())
    for i, queen in enumerate(queens):
        loc = locate_queen(n, i, queen)
        add_queen(tracker, loc, 1)
    return tracker


def gen_conflicts(n, queens, tracker=None):
    if tracker is None:
        tracker = make_tracker(n, queens)
    for i, queen in enumerate(queens):
        loc = locate_queen(n, i, queen)
        yield measure_conflicts(tracker, loc)

def compute_obj(n, queens):
    return sum(gen_conflicts(n, queens))

def make_initial_state(n, queens):
    obj = compute_obj(n, queens)
    return State(obj, n, normalise_queens(queens), depth=0)


def apply_move(move, queens):
    queens_prime = list(queens)
    queens_prime[move.i] = move.j_dst
    return tuple(queens_prime)

def make_succ_state(state, queens_prime, move):
    obj_prime = state.obj + move.obj_delta
    return State(obj_prime, state.n, normalise_queens(queens_prime), state.depth + 1)


def delta_objective(tracker, z_src, z_dst):
    """
    compute change in objective value in board state (encoded by tracker)
    if we move a queen from board-coords z_src to board-coords z_dst.

    if the result is negative it means making this move will
    reduce (improve) the objective function.
    """
    delta = 0
    for f_k, z_src_k, z_dst_k in izip(tracker, z_src, z_dst):
        if z_src_k == z_dst_k:
            continue
        else:
            delta += 2 * f_k[z_dst_k] - 2 * f_k[z_src_k] + 2
    return delta


AMove = namedtuple('AMove', ['obj_delta', 'i', 'j_src', 'j_dst', 'z_src', 'z_dst'])


def gen_evaluated_moves(tracker, state):
    """
    with a little algebraic insight (hopefully)

    this itself is an interesting optimisation subproblem:

    given the constant count vectors in tracker
    and the locations of the n queens
    """
    tracker = tracker or make_tracker(state.n, state.queens)

    # use a couple of random orders for i and j_dst
    # (we could re-randomise j_dst each outer loop, but that isnt free)
    queen_order = range(state.n)
    random.shuffle(queen_order)

    j_dst_order = range(state.n)
    random.shuffle(j_dst_order)

    # consider moving queen in row i from col j_src to col j_dst

    for i in queen_order:
        j_src = state.queens[i]
        z_src = locate_queen(state.n, i, j_src)

        for j_dst in j_dst_order:
            if j_src == j_dst:
                continue
            z_dst = locate_queen(state.n, i, j_dst)
            delta = delta_objective(tracker, z_src, z_dst)
            if delta > 0:
                continue # xxx hack : NEVER SETTLE FOR LESS. local optimality or bust.
            yield AMove(delta, i, j_src, j_dst, z_src, z_dst)


def semi_sorted(is_minimal, moves, max_q_size=10000):
    q = []
    for move in moves:
        heappush(q, move)
        while (q and is_minimal(q[0])) or (len(q) >= max_q_size):
            yield heappop(q)
    while q:
        yield heappop(q)


def search(n, initial_state, goal_obj, tracker_cache, max_tracker_cache=1):

    def prioritised(state):
        return state.obj, -state.depth, state

    def is_goal(state):
        return state.obj <= goal_obj

    closed = set(initial_state.queens)
    q = [prioritised(initial_state)]
    while q:
        _, _, state = heappop(q)
        print 'obj=%d\t\t|q| = %d\t\tdepth %d' % (state.obj, len(q), state.depth)
        # print '\tstate.queens %s' % str(state.queens)
        if is_goal(state):
            return state

        is_locally_optimal = lambda move : move.obj_delta < 0

        if state.queens not in tracker_cache: # weirdness
            if len(tracker_cache) > max_tracker_cache:
                tracker_cache.popitem()
            tracker_cache[state.queens] = make_tracker(state.n, state.queens)
        tracker = tracker_cache.get(state.queens)

        for move in chain(semi_sorted(is_locally_optimal, gen_evaluated_moves(tracker, state))):
            queens_prime = apply_move(move, state.queens)
            if queens_prime in closed:
                continue
            succ_state = make_succ_state(state, queens_prime, move)
            if is_goal(succ_state): # sidestep huge branching factor!
                if len(tracker_cache) > max_tracker_cache:
                    tracker_cache.popitem()
                tracker_cache[succ_state.queens] = update_tracker(tracker, move.z_src, move.z_dst)
                return succ_state
            closed.add(succ_state.queens)
            heappush(q, prioritised(succ_state))
    return None


def jungle_queen(n):

    queens = range(n)

    random.seed(0)
    random.shuffle(queens)

    initial_state = make_initial_state(n, queens)

    print 'initial queens = %s' % str(queens)
    print 'initial state objective = %d' % initial_state.obj

    state = initial_state
    best_obj = state.obj

    tracker_cache = {}

    while best_obj > 4 * n:
        print '*** best_obj = %d ***' % best_obj
        state = search(n, state, best_obj-1, tracker_cache)
        assert state is not None
        best_obj = state.obj

    soln_state = state
    return soln_state.queens

